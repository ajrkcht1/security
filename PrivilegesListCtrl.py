__author__ = 'shachar'

from ListCtrl import ListCtrl
import wx


#------------Start of Class <PrivilegesListCtrl>------------
class PrivilegesListCtrl(ListCtrl):
    # -------Constant Variables-------
    STATUS_DISABLED = "Disabled"
    STATUS_ENABLED  = "Enabled"
    OPTION_ENABLE   =  "Enable"
    OPTION_DISABLE  =  "Disable"
    #---------------------------------

    def __init__(self, func_on_selected, *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)
        self.new_frame           = None
        self.selection_privilege = ""
        self.func_on_selected    = func_on_selected

    def on_right_up(self, event):
        """
            When user select item with right click.
            Open a window of options and than
            Call the function 'func_on_selected'.
            Edit self.selection_process and self.new_frame.
        """
        raw, pos, values         = self.get_info_from_event(event)
        self.selection_privilege = values
        status = values[-1].split(" ")[0]
        if status == PrivilegesListCtrl.STATUS_DISABLED:
            option = PrivilegesListCtrl.OPTION_ENABLE
        elif status == PrivilegesListCtrl.STATUS_ENABLED:
            option = PrivilegesListCtrl.OPTION_DISABLE
        else:
            raise Exception("Status is not Disabled or Enabled")

        self.new_frame           = self.start_options_screen(event, [option])

    def on_select(self, selection, event=None):
        """
            Call the appropriate functions when selected.
        """
        return self.func_on_selected(selection, self.selection_privilege, event=event)

    def kill_listBox(self, event):
        """
            Kill listBox.
        """
        try:
            if self.new_frame is not None:
                self.new_frame.Destroy()
                self.new_frame = None
        except wx.PyDeadObjectError:
            self.new_frame = None
        event.Skip()
#------------End of Class <PrivilegesListCtrl>------------
#------------End of Code------------