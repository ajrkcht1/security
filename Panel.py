__author__ = 'Shachar'

import wx
from ListCtrl import ListCtrl
from PrivilegesListCtrl import PrivilegesListCtrl
from ProcessesListCtrl import ProcessesListCtrl


#------------Start of Class <Panel>------------
class Panel(wx.Panel):
    def __init__(self, func_on_selected, refresh_func, *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)
        # Default Values:
        self.func_on_selected    = func_on_selected
        self.refresh_func        = refresh_func
        self.listCtrl_processes  = None
        self.listCtrl_privileges = None
        self.listCtrl_system     = None
        self.refresh_button      = None
        self.sizer_above_panel   = None
        self.main_sizer          = None

        self.__set_panel()

    def __set_panel(self):
        """
            Build Panel and set:
                self.sizer_above_panel, self.main_sizer
        """
        self.sizer_above_panel = wx.BoxSizer(wx.VERTICAL)
        self.main_sizer        = wx.BoxSizer(wx.VERTICAL)
        self.__set_main_sizer()
        self.sizer_above_panel.Add(self.main_sizer, proportion=1, flag=wx.EXPAND, border=0)
        self.SetSizer(self.sizer_above_panel)
        self.Layout()
        self.sizer_above_panel.Fit(self)

    def __set_main_sizer(self):
        """
            Set all widgets and sizers into the main sizer.
            Edit self.listCtrl_processes, self.listCtrl_privileges, self.listCtrl_system, self.refresh_button.
        """
        self.build_listCtrl_processes()
        self.build_listCtrl_privileges()
        self.build_listCtrl_system()
        self.build_refresh_button()
        self.add_to_sizers()

    # -------Event Functions-------
    def on_left_up_refresh(self, event):
        """
            When refresh button is clicked.
        """
        self.refresh_func()
        event.Skip()

    # -------Build Functions-------
    def build_listCtrl_processes(self):
        """
            Build listCtrl of Processes.
        """
        self.listCtrl_processes = ProcessesListCtrl(self.func_on_selected, self, style=wx.LC_REPORT | wx.LC_VRULES, name="processes")
        self.listCtrl_processes.SetForegroundColour(wx.Colour(0, 0, 255))
        self.listCtrl_processes.SetBackgroundColour(wx.Colour(0, 0, 0))

    def build_listCtrl_privileges(self):
        """
            Build listCtrl of Privileges.
        """
        self.listCtrl_privileges = PrivilegesListCtrl(self.func_on_selected, self, style=wx.LC_HRULES | wx.LC_REPORT | wx.LC_VRULES, name="privileges")
        self.listCtrl_privileges.SetForegroundColour(wx.Colour(255, 128, 0))
        self.listCtrl_privileges.SetBackgroundColour(wx.Colour(0, 0, 0))

    def build_listCtrl_system(self):
        """
            Build listCtrl of System.
        """
        self.listCtrl_system       = ListCtrl(self, style=wx.LC_HRULES | wx.LC_REPORT, name="system")
        self.listCtrl_system.SetForegroundColour(wx.Colour(255, 0, 0))
        self.listCtrl_system.SetBackgroundColour(wx.Colour(0, 0, 0))

    def build_refresh_button(self):
        """
            Build Refresh Button.
        """
        self.refresh_button = wx.Button(self, label="Refresh", style=wx.BU_EXACTFIT | wx.NO_BORDER)
        self.refresh_button.SetForegroundColour(wx.Colour(51, 209, 255))
        self.refresh_button.SetBackgroundColour(wx.Colour(0, 0, 0))
        self.refresh_button.Bind(wx.EVT_LEFT_UP, self.on_left_up_refresh)

    # -------Sizer Functions-------
    def add_to_sizers(self):
        """
            Add all items to the sizers.
        """
        # Add System and Privileges to a Sizer:
        sizer_privileges_and_system = wx.BoxSizer(wx.HORIZONTAL)
        sizer_privileges_and_system.Add(self.listCtrl_privileges, proportion=1, flag=wx.EXPAND, border=0)
        sizer_privileges_and_system.Add(self.listCtrl_system, proportion=0, flag=wx.EXPAND, border=0)
        sizer_privileges_and_system.Add(self.refresh_button, proportion=0, flag=wx.EXPAND | wx.BOTTOM | wx.RIGHT, border=1)

        # Add all items to the main sizer:
        self.main_sizer.Add(self.listCtrl_processes, 1, wx.EXPAND, 0)
        self.main_sizer.Add(sizer_privileges_and_system, proportion=0, flag=wx.EXPAND, border=0)
#------------End of Class <Panel>------------
#------------End of Code------------