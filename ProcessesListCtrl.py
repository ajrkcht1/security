__author__ = 'shachar'

from ListCtrl import ListCtrl
import wx


#------------Start of Class <ProcessesListCtrl>------------
class ProcessesListCtrl(ListCtrl):
    # -------Constant Variables-------
    SUSPEND = "Suspend"
    RESUME  = "Resume"
    KILL    = "Kill"
    #---------------------------------

    def __init__(self, func_on_selected, *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)
        self.new_frame         = None
        self.selection_process = ""
        self.func_on_selected  = func_on_selected

        self.Bind(wx.EVT_LIST_ITEM_SELECTED, self.on_select_no_right_click)

    def on_right_up(self, event):
        """
            When user select item with right click
            Open a window of options and than
            Call the function 'func_on_selected'
            Edit self.selection_process and self.new_frame
        """
        raw, pos, values       = self.get_info_from_event(event)
        self.selection_process = values
        status                 = values[-1]
        if status == "stopped":
            option = ProcessesListCtrl.RESUME
        elif status == "running":
            option = ProcessesListCtrl.SUSPEND
        else:
            raise Exception("Status is not 'running' or 'stopped'")
        self.new_frame         = self.start_options_screen(event, [ProcessesListCtrl.KILL, option])

    def on_select(self, selection, event=None):
        """
            Call the appropriate functions when selected.
        """
        return self.func_on_selected(selection, self.selection_process, event=event)

    def kill_listBox(self, event):
        """
            Kill listBox.
        """
        try:
            if self.new_frame is not None:
                self.new_frame.Destroy()
                self.new_frame = None
        except wx.PyDeadObjectError:
            self.new_frame = None
        event.Skip()

    def on_select_no_right_click(self, event):
        raw, pos, values = self.get_info_from_event(event)
        self.func_on_selected("", values, right_click=False)
        event.Skip()
#------------End of Class <ProcessesListCtrl>------------
#------------End of Code------------