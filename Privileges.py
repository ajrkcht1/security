__author__ = 'Shachar'

import os
import win32security
from Privilege import Privilege


#------------Start of Class <Privileges>------------
class Privileges(object):

    def __init__(self, process, log):
        self.log        = log
        self.process    = process
        self.privileges = self.__get_privileges()

    def __get_privileges(self):
        """
            Return list of all privileges of the process
        """
        privileges = []
        for privilege_name, privilege_flag in self.process.token_information:
            privileges.append(Privilege(privilege_name, privilege_flag))
        return privileges

    def add_privilege(self, new_privilege_name):
        """
            Get a new_privilege and add/toggle it (update self.token_information and self.privileges)
        """
        self.log.info("add privilege {new_privilege}".format(new_privilege=new_privilege_name))    # Debug
        privilege_id     = win32security.LookupPrivilegeValue("", new_privilege_name)
        privileges_names = [privilege.name for privilege in self.privileges]
        if new_privilege_name in privileges_names:
            self.__toggle_privilege(privilege_id)
        else:
            self.__append_privilege(privilege_id)

    def __toggle_privilege(self, param_name_id):
        """
            Get an ID of privilege and toggle its flag
        """
        self.log.debug("toggle: privilege_id = {param_id}".format(param_id=param_name_id))  # Debug
        new_token_information = []
        for privilege in self.privileges:
            if privilege.name_id == param_name_id:
                    privilege.flag_id ^= 0b10
            new_token_information.append((privilege.name_id, privilege.flag_id))
        self.__update_all_privileges(tuple(new_token_information))

    def __update_all_privileges(self, new_token_information):
        """
            Update/Change to the new privileges and 
            update the necessary attributes (self.process.token_information and self.privileges)
        """
        self.log.debug("1- Old Privileges: \n{token_information}".format(token_information=self.process.token_information))             # Debug
        self.log.debug("2- Want to Change to: {new_token_information}".format(new_token_information=new_token_information))             # Debug
        win32security.AdjustTokenPrivileges(self.process.token_handler, 0, new_token_information)
        self.process.token_information = win32security.GetTokenInformation(self.process.token_handler, win32security.TokenPrivileges)   # Debug
        self.log.debug("3- Changed to: {token_information}".format(token_information=self.process.token_information))                   # Debug
        self.privileges = self.__get_privileges()   # Update the self.privileges value

    def __append_privilege(self, privilege_id):
        """
            Add a new privilege that not exists
        """
        self.log.debug("append: privilege_id = {privilege_id}".format(privilege_id=privilege_id))   # Debug
        #TODO Add new privilege to process (There is no such thing in python, so the function doesn't work)
        new_token_information = list(self.process.token_information)
        new_token_information.append((privilege_id, win32security.SE_PRIVILEGE_ENABLED))
        self.__update_all_privileges(tuple(new_token_information))

    def __str__(self):
        return self.to_string()

    def to_string(self, separator=False):
        """
            Get a Process and return a string of its privileges
        """
        output = ""
        if separator:
            output += '-'*80 + os.linesep
        for privilege in self.privileges:
            output += "{name}: {flag}".format(name=privilege.name, flag=privilege.flag) + os.linesep
        return output

    @staticmethod
    def get_values_name():
        """
            Headers for GUI.
        """
        return ["Privilege", "Status"]

    def get_values(self):
        """
            Values for GUI.
        """
        return [[privilege.name, privilege.flag] for privilege in self.privileges]
#------------End of Class <Privileges>------------
#------------End of Code------------
