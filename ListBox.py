__author__ = 'shachar'

import wx


#------------Start of Class <ListBox>------------
class ListBox(wx.ListBox):
    def __init__(self, func_on_selected, event, frame, style=wx.LB_SINGLE, *args, **kwargs):
        super(self.__class__, self).__init__(frame, style=style, *args, **kwargs)
        self.func_on_selected = func_on_selected
        self.event            = event
        self.frame            = frame
        self.index            = 0
        # Connect event:
        self.Bind(wx.EVT_LISTBOX, self.on_selected)

    def insert_option(self, option):
        """
            Insert option to the ListBox.
        """
        self.Insert(option, self.index)
        self.index += 1

    def on_selected(self, event):
        """
            When an options is selected.
        """
        selection = event.GetString()
        self.frame.Close()
        self.func_on_selected(selection, self.event)
#------------End of Class <ListBox>------------
#------------End of Code------------