# -*- coding: CP1255 -*-
__author__ = 'Shachar'

import wx
from Panel import Panel


#------------Start of Class <InitialFrame>------------
class InitialFrame(wx.Frame):
    # -------Constant Variables-------
    DEFAULT_SIZE = wx.Size(1050, 800)
    #---------------------------------

    def __init__(self, func_on_selected, refresh_func, parent, id=wx.ID_ANY, title=wx.EmptyString, pos=wx.DefaultPosition,
                 size=DEFAULT_SIZE, style=wx.DEFAULT_FRAME_STYLE, name=wx.FrameNameStr):
        """
            __init__(self, Function func_on_selected, Function refresh_func, Window parent, int id=-1, String title=EmptyString,
            Point pos=DefaultPosition, Size size=DefaultSize,
            long style=DEFAULT_FRAME_STYLE, String name=FrameNameStr) -> Frame
        """
        super(self.__class__, self).__init__(parent, id, title, pos, size, style, name)
        self.main_panel       = None
        self.func_on_selected = func_on_selected
        self.refresh_func     = refresh_func
        self.title            = title

        self.__set_properties()
        self.__do_layout()

    def __set_properties(self):
        """
            Set the properties of the frame.
        """
        self.SetTitle(self.title)

    def __do_layout(self):
        """
            Build first layout.
            Edit self.main_panel
        """

        # Initial sizers and panel
        self.SetSizeHintsSz(wx.Size(750, 450), wx.DefaultSize)   # Minimum size and Maximum size
        sizer_before_panel             = wx.BoxSizer(wx.VERTICAL)
        self.main_panel                = Panel(self.func_on_selected, self.refresh_func, self)

        # Set sizers above and before panel
        sizer_before_panel.Add(self.main_panel, proportion=1, flag=wx.EXPAND | wx.ALL, border=0)

        self.SetSizer(sizer_before_panel)

        self.Layout()
        self.Centre(wx.BOTH)
#------------End of Class <InitialFrame>------------
#------------End of Code------------