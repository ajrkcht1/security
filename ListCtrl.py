__author__ = 'shachar'

import wx
from ListBox import ListBox


#------------Start of Class <ListCtrl>------------
class ListCtrl(wx.ListCtrl):
    # -------Constant Variables-------
    ONE_SELECTION_SIZE = wx.Size(60, 35/2)
    TWO_SELECTION_SIZE = wx.Size(60, 35)

    def __init__(self, parent, id=wx.ID_ANY, pos=wx.DefaultPosition, size=wx.DefaultSize,
                 style=wx.LC_ICON, validator=wx.DefaultValidator, name=wx.ListCtrlNameStr):
        wx.ListCtrl.__init__(self, parent, id, pos, size, style, validator, name)
        self.name      = name
        self.index     = 1      # Counter to determined first column (No.)
        self.positions = {}     # Positions of all raws so they can be edit
        # Connect event:
        self.Bind(wx.EVT_LIST_ITEM_RIGHT_CLICK, self.on_right_up)
        self.Bind(wx.EVT_LEFT_DOWN, self.kill_listBox)
        self.Bind(wx.EVT_RIGHT_DOWN, self.kill_listBox)

    def build_headers(self, headers):
        """
            Build the headers accordingly to the list of headers by the order we got.
        """
        headers = ["No."] + headers
        for i in xrange(len(headers)):
            self.InsertColumn(i, headers[i])
        self.Arrange()

    def append(self, values):
        """
            Append an item to the list control (to the end).
            The values should be a sequence with an item for each column.
        """
        self.Append([str(self.index)] + values)
        self.index += 1

    def append_and_save_pos(self, values):
        """
            Append an item to the list control (to the end).
            The values should be a sequence with an item for each column.
            Save all positions that were added, positions[raw] => (position, values) .
        """
        pos = self.InsertStringItem(self.index - 1, str(self.index))
        self.__set_values(pos, values)
        self.positions[self.index] = (pos, values)
        self.index += 1

    def update_raw(self, raw, new_values):
        """
            Update the raw (raw start from one).
            The values should be a sequence with an item for each column.
        """
        pos = self.positions[raw][0]
        self.__set_values(pos, new_values)
        self.positions[raw] = (pos, new_values)

    def __set_values(self, pos, values):
        """
            Set values to a given position.
            The values should be a sequence with an item for each column.
        """
        for i in xrange(len(values)):
            self.SetStringItem(pos, i + 1, values[i])

    def on_right_up(self, event):
        """
            This function can be override by child.
        """
        event.Skip()

    def get_info_from_event(self, event):
        """
            Get an event (EVT_LIST_ITEM_RIGHT_CLICK).
            Return the raw, pos and values of the selected item.
        """
        raw         = event.GetIndex() + 1
        pos, values = self.positions[raw]
        return raw, pos, values

    def start_options_screen(self, event, options):
        """
            Open a window of options (can be one or two options).
            Return new frame.
        """
        assert not len(options) > 2
        size                     = ListCtrl.ONE_SELECTION_SIZE if len(options) == 1 else ListCtrl.TWO_SELECTION_SIZE
        x_screen, y_screen       = self.GetScreenPosition()
        x_mouse_pos, y_mouse_pos = event.GetPoint()
        new_frame  = wx.Frame(parent=None, pos=(x_mouse_pos+x_screen, y_mouse_pos+y_screen), size=size, style=wx.STAY_ON_TOP)
        list_box   = ListBox(func_on_selected=self.on_select, event=event, frame=new_frame)
        for option in options:
            list_box.insert_option(option)
        new_frame.Show()
        return new_frame

    def kill_listBox(self, event):
        """
            This function can be override by child to kill listBox.
        """
        event.Skip()
#------------End of Class <ListCtrl>------------
#------------End of Code------------