__author__ = 'Shachar'

import wx
from InitialFrame import InitialFrame


#------------Start of Class <App>------------
class App(wx.PySimpleApp):
    # -------Constant Variables-------
    TITLE_WINDOW = "Task Manager V1.0"
    #---------------------------------

    def __init__(self, func_on_selected, refresh_func, redirect=False, filename=None, useBestVisual=False, clearSigInt=True):
        super(self.__class__, self).__init__(redirect, filename, useBestVisual, clearSigInt)
        self.func_on_selected = func_on_selected
        wx.InitAllImageHandlers()
        self.frame = InitialFrame(self.func_on_selected, refresh_func, None, title=App.TITLE_WINDOW)
        self.__on_init()

    def __on_init(self):
        """
            On initialing this script run (overriding PySimpleApp.OnInit).
        """
        self.SetTopWindow(self.frame)
        self.frame.Show()
        return True
#------------End of Class <App>------------
#------------End of Code------------