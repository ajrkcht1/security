__author__ = 'Shachar'

import os
import psutil
import win32security
import win32api
import win32con
from Privileges import Privileges
from log import Log


#------------Start of Class <Process>------------
class Process(object):
    # -------Constant Variables-------
    PROCESS_TERMINATE    = 1
    ACCESS_ERROR         = "No Access"
    ACCESS_VARIABLE_NAME = "access"
    PID_ERROR            = "No Such Process"
    PID_VARIABLE_NAME    = "is_such"
    FORBIDDEN_FUNCTIONS  = ["add_privilege", "privileges_to_string", "suspend", "resume", "kill"] # Forbidden if no access or such name
    #---------------------------------

    def __init__(self, pid, logging_level=Log.CRITICAL):
        # Utilities-Variables:
        filename               = "{class_name}_{pid}".format(class_name="Process", pid=pid)     # Filename for Logging
        self.log               = Log(filename=filename, logging_level=logging_level,class_name=__name__).log
        # Win32Api-Variables:
        self.access, self.is_such, self.process_handler, self.token_handler, self.token_information = self.__get_win32api_variables(pid)
        # PSUTIL-Variables:
        self.psutil_handle     = psutil.Process(pid)
        # Info-Variables:
        dic_info               = self.psutil_handle.as_dict(attrs=["pid", "status", "memory_percent", "cpu_percent", "create_time", "exe", "name", "num_handles", "num_threads", "username"], ad_value="AccessDenied")
            # Constant-Info-Variables:
        self.pid               = dic_info["pid"]
        self.name              = dic_info["name"]
        self.username          = dic_info["username"]
        self.exe               = dic_info["exe"]
        self.create_time       = dic_info["create_time"]
            # Change-Info-Variables:    (Change during time, every variable has get-function)
        self.status            = dic_info["status"]
        self.memory_percent    = dic_info["memory_percent"]
        self.cpu_percent       = dic_info["cpu_percent"]
        self.num_handles       = dic_info["num_handles"]
        self.num_threads       = dic_info["num_threads"]

        self.privileges        = Privileges(self, self.log)     # Must be last attribute (using Process's attributes)

    def __get_win32api_variables(self, pid):
        """
            Return all Win32Api-Variables
        """
        access            = False
        is_such           = False
        process_handler   = None
        token_handler     = None
        token_information = []
        try:
            process_handler   = win32api.OpenProcess(win32con.PROCESS_QUERY_INFORMATION, False, pid)
            token_handler     = win32security.OpenProcessToken(process_handler, win32security.TOKEN_ALL_ACCESS)
            token_information = win32security.GetTokenInformation(token_handler, win32security.TokenPrivileges)
            access  = True
            is_such = True
        except Exception, e:
            if "(5, 'OpenProcess'" in str(e):
                is_such = True
                self.log.info("No Access to Process {pid}".format(pid=pid))
            elif "(87, 'OpenProcess'" in str(e):
                access = True
                self.log.info("No Such Process {pid}".format(pid=pid))
            else:
                raise e
        return access, is_such, process_handler, token_handler, token_information

    # -------ToString-Functions-------
    def privileges_to_string(self, separator=False):
        """
            Return info of all privileges
        """
        return self.privileges.to_string(separator)

    def to_string(self):
        output  = "\t###->> {name} <<-###".format(name=self.name) + os.linesep
        output += "pid: {pid}{sep}username: {username}{sep}exe: {exe}{sep}".format(pid=self.pid, username=self.username.encode('utf-8'), exe=self.exe, sep=os.linesep)
        output += "create_time: {create_time}{sep}status: {status}{sep}".format(create_time=self.create_time, status=self.status, sep=os.linesep)
        output += "memory_percent: {memory_percent}{sep}cpu_percent: {cpu_percent}{sep}".format(memory_percent=self.memory_percent, cpu_percent=self.cpu_percent, sep=os.linesep)
        output += "num_handles: {num_handles}{sep}num_threads: {num_threads}{sep}".format(num_handles=self.num_handles, num_threads=self.num_threads, sep=os.linesep)
        output += "Privileges:{sep}".format(sep=os.linesep)
        output += self.privileges_to_string() + os.linesep
        output += "-"*80
        return output

    def __str__(self):
        return self.to_string()

    #  -------Command-Functions-------
    def add_privilege(self, new_privilege_name):
        """
            Add Privilege to a Process
        """
        self.privileges.add_privilege(new_privilege_name)

    def suspend(self):
        """
            Suspend this process
        """
        self.psutil_handle.suspend()

    def resume(self):
        """
            Resume this process (after it was suspend)
        """
        self.psutil_handle.resume()

    def kill(self):
        """
            Kill this Process
        """
        process_handle_to_exit = win32api.OpenProcess(Process.PROCESS_TERMINATE, False, self.pid)
        win32api.TerminateProcess(process_handle_to_exit, -1)
        win32api.CloseHandle(process_handle_to_exit)
        win32api.CloseHandle(self.process_handler)
        win32api.CloseHandle(self.token_handler)

    #  -------Get-Functions-------
    def get_status(self):
        self.status = self.psutil_handle.status()
        return self.status

    def get_memory_percent(self):
        self.memory_percent = self.psutil_handle.memory_percent()
        return self.memory_percent

    def get_cpu_percent(self):
        self.cpu_percent = self.psutil_handle.cpu_percent()
        return self.cpu_percent

    def get_num_handles(self):
        self.num_handles = self.psutil_handle.num_handles()
        return self.num_handles

    def get_num_threads(self):
        self.num_threads = self.psutil_handle.num_threads()
        return self.num_threads

    @staticmethod
    def get_info_names():
        """
            Headers for GUI.
        """
        return ["name", "pid", "username", "exe", "create_time", "cpu_percent", "memory_percent", "num_handles", "num_threads", "status"]

    def get_info_values(self):
        """
            Values for GUI.
        """
        values = [self.name, self.pid, self.username, self.exe, self.create_time, self.cpu_percent,
                  self.memory_percent, self.num_handles, self.num_threads, self.status]
        return map(lambda value: value if type(value) is str or type(value) is unicode else str(value), values)  # Change all to string

    def __getattribute__(self, attr):
        """
            If there is no access/such-name, it's overriding the call for the function and return the appropriate message.
        """
        get_attr         = lambda self, attr: object.__getattribute__(self, attr)   # Return real attr
        get_access_error = lambda *arg, **kwargs: Process.ACCESS_ERROR              # Return Access-Error
        get_pid_error    = lambda *arg, **kwargs: Process.PID_ERROR                 # Return No_Such_Pid-Error
        if str(attr) == Process.PID_VARIABLE_NAME or str(attr) == Process.ACCESS_VARIABLE_NAME:
            return get_attr(self, attr)
        if str(attr) in Process.FORBIDDEN_FUNCTIONS:
            if not self.is_such:
                return get_pid_error
            elif not self.access:
                return get_access_error
        return get_attr(self, attr)
#------------End of Class <Process>------------
#------------End of Code------------