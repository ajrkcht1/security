__author__ = 'shachar'

import os
import psutil
from App import App
from ProcessesListCtrl import ProcessesListCtrl
from PrivilegesListCtrl import PrivilegesListCtrl
from log import Log
from Process import Process
from Privileges import Privileges


#------------Start of Class <Controller>------------
class Controller(object):
    # -------Constant Variables-------
    SYSTEM_HEADERS = ["Variable", "Parameter"]
    #---------------------------------

    def __init__(self, logging_level=Log.CRITICAL):
        # Utility-Variables:
        self.logging_level = logging_level
        # MODEL-Variables:
        self.processes     = self.get_processes()
        # VIEW-Variables:
        self.app           = App(self.func_on_selected, self.refresh)
        self.frame         = self.app.frame
        self.panel         = self.frame.main_panel
            # Get Widgets:
        self.listCtrl_processes  = self.panel.listCtrl_processes
        self.listCtrl_privileges = self.panel.listCtrl_privileges
        self.listCtrl_system     = self.panel.listCtrl_system
        self.selected_process    = None

    # --------------MODEL--------------
    def get_processes(self):
        """
            Return dictionary of processes by pid.
        """
        processes = {}
        for pid in psutil.pids():
            try:
                processes[pid] = Process(pid, self.logging_level)
            except psutil.NoSuchProcess:
                pass
        return processes

    # --------------VIEW--------------
    def main_loop(self):
        """
            Run the mainloop of the GUI.
        """
        self.app.MainLoop()

    # -------Build Functions-------
    def build_processes(self):
        """
            Build the list-ctrl of the processes and 10% empty places to GUI (sorted by name).
        """
        headers = Process.get_info_names()
        self.listCtrl_processes.build_headers(headers)
        for process in sorted(self.processes.values(), key=lambda proc: proc.name.upper()):
            self.listCtrl_processes.append_and_save_pos(process.get_info_values())
        for i in xrange(int(len(self.processes.values())*0.1)):
            self.listCtrl_processes.append_and_save_pos([""]*10)

    def build_privileges(self, process=None):
        """
            On initial build 10 empty places of privileges.
            If process is given, build its privileges.
        """
        if process is None:     # On Initial
            self.listCtrl_privileges.build_headers(Privileges.get_values_name())
            for value in ["", ""]*10:
                self.listCtrl_privileges.append_and_save_pos(value)
        else:
            values = process.privileges.get_values()
            for i in xrange(20):
                if i < len(values):
                    self.listCtrl_privileges.update_raw(i+1, values[i])
                else:
                    self.listCtrl_privileges.update_raw(i+1, ["", ""])

    def build_system(self):
        """
            Build system environment variables to list-Ctrl.
        """
        self.listCtrl_system.build_headers(Controller.SYSTEM_HEADERS)
        for key, value in os.environ.items():
            self.listCtrl_system.append_and_save_pos([key, value])

    def func_on_selected(self, selection, selection_values, right_click=True, event=None):
        if not right_click and selection == "":     # if process selected without right-click
            self.set_process_privileges(selection_values)
        elif selection == ProcessesListCtrl.SUSPEND or selection == ProcessesListCtrl.RESUME or selection == ProcessesListCtrl.KILL:    # If it's process
            self.execute_command(selection, selection_values)
        else:
            self.add_privilege(selection, selection_values)

    # -------Execute-Functions-------
    def refresh(self):
        """
            Refresh all processes data.
        """
        self.processes = self.get_processes()
        inedx          = 1
        for process in sorted(self.processes.values(), key=lambda proc: proc.name.upper()):
            self.listCtrl_processes.update_raw(inedx+1, process.get_info_values())
            inedx += 1

    def set_process_privileges(self, selection_values):
        """
            Change to process's privileges and set the selected process.
        """
        pid                   = int(selection_values[1])
        process               = self.processes[pid]
        self.build_privileges(process)
        self.selected_process = process

    def execute_command(self, selection, selection_values):
        """
            Get the user's selection and execute the appropriate command.
        """
        pid     = int(selection_values[1])
        process = self.processes[pid]
        if selection == ProcessesListCtrl.SUSPEND:
            process.suspend()
            self.refresh()
        elif selection == ProcessesListCtrl.RESUME:
            process.resume()
            self.refresh()
        elif selection == ProcessesListCtrl.KILL:
            process.kill()
            self.listCtrl_processes.DeleteItem(self.listCtrl_processes.GetFocusedItem())
            self.processes.pop(pid)
        else:
            raise Exception("No such selection " + selection)

    def add_privilege(self, selection, selection_values):
        """
            Add privilege to the selected process (Enable/Disable).
            Update the GUI appropriately.
        """
        if selection == PrivilegesListCtrl.OPTION_ENABLE or selection == PrivilegesListCtrl.OPTION_DISABLE:
            self.selected_process.add_privilege(selection_values[0])
            self.build_privileges(self.selected_process)
        else:
            raise Exception("No such Selection in privileges" + selection)
#------------End of Class <Controller>------------


#------------Main------------
def main():
    """
        Main Function- run the controller and build the GUI.
    """
    controller = Controller()
    controller.build_processes()
    controller.build_privileges()
    controller.build_system()
    controller.main_loop()

if __name__ == "__main__":
    main()
#------------End of Code------------